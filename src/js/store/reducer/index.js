import {combineReducers} from 'redux';
import { connectRouter } from 'connected-react-router';

import user from './user';
import History from '../../routes/history';

export default combineReducers({
    user,
    router: connectRouter(History),
});