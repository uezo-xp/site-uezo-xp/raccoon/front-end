import *  as TiposAcao from '../actions';

const INICIAL_STATE = {
    nome: (localStorage.getItem('user')!== null)?(localStorage.getItem('user')):(''),
    error: '',
    listaAtualizada: false
}

export default function user(state = INICIAL_STATE, action)
{
    switch(action.type)
    {
        case TiposAcao.LOGIN:
            localStorage.setItem('user', action.login);
            return {
                ...state,
                nome: action.login,
                error: ''
            } 
        case TiposAcao.LOGOUT:
            localStorage.clear();
            return {
                ...state,
                nome:'',
                error:''
            }
        case TiposAcao.ENVIOU_POST:
            return{
                ...state,
                listaAtualizada:false
            }
        case TiposAcao.ATUALIZOU_POST:
            return{
                ...state,
                listaAtualizada:true
            }
        case TiposAcao.ERRO:
            return{
                ...state,
                error: action.erro
            }
        default:
            return state;
    }
    
}