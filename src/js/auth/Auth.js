import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const isAuth = () => {
    if(localStorage.getItem('user') !== null) {
        return true
    }
    return false;
};

const AlternativeRoute = ({component: Component, ...rest}) => {
   return (
       <Route 
           {...rest}
           render={props => 
           isAuth() ? (
               <Component {...props} />
           ):(
               <Redirect 
                   to={{
                       pathname: '/home'
                   }}
               />
           )}
       />
   );
}

export default AlternativeRoute;
