import React from "react";

import Header from '../Componentes/Header';

import Profile from '../pages/Profile';
import Logout from '../pages/Logout';
import Home from '../pages/Home';
import History from './history';

//import AlternativeRoute from '../auth/Auth';

import { ConnectedRouter } from "connected-react-router";
import { Route, Switch} from "react-router-dom";

const Routes = () => (
  <ConnectedRouter history={History} > 
   <div className={'container'} style={{width:'100%'}}>
    <Header title={'Uxp'}/>
      <div className={'col-12'}>
          <Switch>
            <Route path="/" component={Profile} />
            <Route path="/home" component={Home}/>
            <Route exact path="/logout" component={Logout}/>
          </Switch>
      </div>
    </div>
  </ConnectedRouter>
);

export default Routes;