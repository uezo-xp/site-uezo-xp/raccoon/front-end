import React from "react";
import LittleCard from '../../Componentes/LittleCard';
import {Spinner} from 'react-bootstrap' ;
import CaixaDePostagem from '../../Componentes/CaixaDePostagem';
import {connect} from 'react-redux';
import * as TiposAcoes from '../../store/actions';
const REACT_APP_API_UXP = process.env.REACT_APP_API_UXP;

class Profile extends React.Component{
    constructor(props)
    {
        super(props);
        this.state = {
            projetosCarregados: []
        }
    }
    BuscandoProjetos = () => {
        const requestInfo = {
            method: 'GET',
            body: JSON.stringify(),
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
        };
        fetch(REACT_APP_API_UXP + '/projetos', requestInfo)
        .then(response => response.json())
        .then(resultado => {
            if(resultado.length > 0)
            {
                this.setState({
                    ...this.state,
                    projetosCarregados: resultado
                });
                    this.props.AtualizouPost();
                return true;
            }
        throw new Error("Houve um erro ao carregar os projetos...");
        })
        .catch(e => {
            this.props.ErroMensagem(e.message);
         }); 
    }   
    render(){
        if(!this.props.listaAtualizada)
            this.BuscandoProjetos();
          
        return(
            <div>
                 <div className='row'>
                     {(this.props.user !== '')?(<CaixaDePostagem user={this.props.user} />):('')}
                    <br/>
                    {(this.props.listaAtualizada)?(
                        this.state.projetosCarregados.map((projeto, index) =>{
                        return <LittleCard key={index} title={projeto.titulo} text={projeto.descricao} autor={projeto.autor} />
                    })):(
                        <div className='col-12' style={{alignItems: 'center', display:'flex', justifyContent:'center'}} >
                            <Spinner variant='success' as="span" animation="border" role="status" aria-hidden="true" />
                        </div>
                    )}
                </div>
            </div>
        );
    }
}
const MapStateToProps = state =>{
    return {
      user: state.user.nome,
      error: state.user.error,
      listaAtualizada: state.user.listaAtualizada
    };
  };
  const MapDispatchToProps = dispatch => {
    return{
        EnviouPost: () => dispatch({type:TiposAcoes.ENVIOU_POST}),
        AtualizouPost:() => dispatch({type:TiposAcoes.ATUALIZOU_POST}),
        ErroMensagem: (e) => dispatch({type:TiposAcoes.ERRO, erro: e})    
    };
  };
  
  export default connect(MapStateToProps, MapDispatchToProps)(Profile);