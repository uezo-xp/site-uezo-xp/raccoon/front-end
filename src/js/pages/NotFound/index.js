import React from 'react';
import {Alert} from 'react-bootstrap';

export default class NotFound extends React.Component
{
    render()
    {
        return(
            <div>
            <Alert variant="danger" >
                <Alert.Heading>Opa! Deu ruim!</Alert.Heading>
                <p>
                Desculpa, não consegui encontrar a pagina desejada
                </p>
            </Alert>
            </div>
        );
    }
}