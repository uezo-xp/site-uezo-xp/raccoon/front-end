import React from 'react';
import {Alert} from 'react-bootstrap';
import Login from './Login';

const Header = ({ title }) => (
    <Alert style={{width:'100%'}} variant={'info'}>
        <div className={'row'} style={{width:'100%'}}>
            
                <div className={'col-12'}>
                    <div style={{float:'left'}}>{title}</div>
                    <Login/>
                </div>
           
        </div>
    </Alert>
);

export default Header;