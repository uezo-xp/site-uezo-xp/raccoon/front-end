import React from 'react';
import {Form, Button, Card} from 'react-bootstrap' ;
import * as TiposAcoes from '../store/actions';
import {connect} from 'react-redux';
const REACT_APP_API_UXP = process.env.REACT_APP_API_UXP;

class CaixaDePostagem extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            valorDescricao:'',
            valorTitulo:''
        };

        this.IdentificandoEnter = this.IdentificandoEnter.bind(this);
        this.LidandoMudancaTextoLabel = this.LidandoMudancaTextoLabel.bind(this);
        this.LidandoMudancaTextoTitulo =  this.LidandoMudancaTextoTitulo.bind(this);
    }
    Postando = () => {
        const data = { usuario: this.props.user.toString(), descricao: this.state.valorDescricao.toString(), titulo: this.state.valorTitulo.toString() };
        const requestInfo = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
        };

        fetch(REACT_APP_API_UXP + '/projetos/inserir', requestInfo)
        .then(response => response.json())
        .then(resultado => {
            if(resultado.query === true)
            {
                this.props.EnviouPost();
                this.setState({...this.state,valorTitulo:'',valorDescricao: ''});
                return true;
            }

        throw new Error("Erro ao Postar...");
        })
        .catch(e => { 
            alert(e);
        }); 
        this.props.EnviouPost();
    }
    VerificandoPost = () =>
    {
        if(this.state.valorDescricao !== '' && this.state.valorTitulo !== '')
        {
            this.Postando();
        }
    }
    IdentificandoEnter(e)
    {
        if(e.key === 'Enter')
        {
            this.VerificandoPost();
        }
    }
    LidandoMudancaTextoLabel(e)
    {
        this.setState({valorDescricao:e.target.value});
    }
    LidandoMudancaTextoTitulo(e)
    {
        this.setState({valorTitulo:e.target.value});
    }
   render()
   { 
    return (
            <>
                <Card className="col-12">
                    <Card.Body>
                    <Form>
                        <Form.Group controlId='GroupDoTitulo'>
                            <Form.Control placeholder="Titulo" value={this.state.valorTitulo} onChange={this.LidandoMudancaTextoTitulo} onKeyDown={this.IdentificandoEnter} />
                        </Form.Group>
                        <Form.Group controlId='GroupDoLabel'>
                            <Form.Control placeholder="No que está pensando agora?"  as='textarea' rows='3' value={this.state.valorDescricao} onChange={this.LidandoMudancaTextoLabel} onKeyDown={this.IdentificandoEnter} />
                        </Form.Group>
                            <Button style={{float:'right'}} onClick={this.VerificandoPost}>Postar</Button>
                    </Form>
                    </Card.Body>
                </Card>  
            </>
        );
    }
}

const MapStateToProps = state =>{
    return {
      user: state.user.nome,
      error: state.user.error,
      atualizaLista: state.user.atualizaLista
    };
  };
  const MapDispatchToProps = dispatch => {
    return{
        EnviouPost: () => dispatch({type:TiposAcoes.ENVIOU_POST}),
        AtualizouPost:() => dispatch({type:TiposAcoes.ATUALIZOU_POST}),
        ErroMensagem: (e) => dispatch({type:TiposAcoes.ERRO, erro: e})
    };
  };
  
  export default connect(MapStateToProps, MapDispatchToProps)(CaixaDePostagem);