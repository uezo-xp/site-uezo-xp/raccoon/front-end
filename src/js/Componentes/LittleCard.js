import React from 'react';
import {connect} from 'react-redux'
import * as TiposAcoes from '../store/actions';
const REACT_APP_API_UXP = process.env.REACT_APP_API_UXP;

class LittleCard extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = {
            modoEdicao : false,
            tempText: ''
        }
    }
    Deletando = () => {
        const data = { usuario: this.props.user.toString(), titulo: this.props.title.toString() };
            const requestInfo = {
                method: 'POST',
                body: JSON.stringify(data),
                headers: new Headers({
                    'Content-Type': 'application/json'
                }),
            };
    
            fetch(REACT_APP_API_UXP + '/projetos/deletar', requestInfo)
            .then(response => response.json())
            .then(resultado => {
                if(resultado.query === true)
                {
                    //alert('Postagem Excluida com sucesso!');
                    this.props.EnviouPost();
                    return true;
                }
    
            throw new Error("Erro ao Deletar...");
            })
            .catch(e => { 
                alert(e);
            }); 
    }
    Editando = () =>
    {
        const data = { usuario: this.props.user.toString(), titulo: this.props.title.toString(),
                                tituloAntigo:this.props.title.toString(), descricao: this.state.tempText.toString()};
            const requestInfo = {
                method: 'POST',
                body: JSON.stringify(data),
                headers: new Headers({
                    'Content-Type': 'application/json'
                }),
            };
    
            fetch(REACT_APP_API_UXP + '/projetos/editar', requestInfo)
            .then(response => response.json())
            .then(resultado => {
                if(resultado.query === true)
                {
                    //alert('Postagem Editada com sucesso!');
                    this.setState({modoEdicao:false, tempText:''});
                    this.props.EnviouPost();
                    return true;
                }
    
            throw new Error("Erro ao Editar...");
            })
            .catch(e => { 
                alert(e);
            }); 
    }
    render ()
    {/** ({ isOpen }) =>
  <Component pose={isOpen ? 'open' : 'closed'} />*/
        const ehVisivel = (this.props.user === this.props.autor.toString());
        return (
            <>
                <div className="card text-white bg-success container-flex" style={{width: '20rem', minHeight:'15rem', margin: '1rem 1rem 1rem 1rem' }}>
                    <div className="card-header" style={{height:'auto', width:'auto'}}>
                        {this.props.title}
                        
                        {ehVisivel ? 
                            <button className='btn btn-outline-light'
                                type='button'
                                style={{borderRadius:'1rem', float:'right',fontSize:'10px'}}
                                onClick={this.Deletando} >X</button> 
                            : ''}
                    </div>
                    <div className="card-body" style={{height:'100%'}}>
                        {(this.state.modoEdicao)?
                        (
                            <>
                                <textarea className='form-control' defaultValue={this.props.text} onChange={(e)=>this.setState({tempText:e.target.value})} ></textarea>
                                <div style={{float:'right', margin:'1rem'}}>
                                    <button className='btn btn-outline-danger' type='button' 
                                        style={{borderRadius:'1rem',fontSize:'14px'}}
                                        onClick={()=>this.setState({modoEdicao:false, tempText:''})}>Cancelar</button>
                                    <button className='btn btn-info' type='button' 
                                    style={{borderRadius:'1rem', marginLeft:'0.5rem',fontSize:'14px'}}
                                    onClick={this.Editando}>Editar</button>
                                </div>
                            </>
                        ):
                        (
                            <>
                                <p className="card-text" >{this.props.text}</p>
                                {(ehVisivel)?( <button className='btn btn-outline-light' type='button' 
                                    style={{borderRadius:'1rem', float:'right',fontSize:'14px'}}
                                    onClick={()=>this.setState({modoEdicao:true})} >Editar</button>
                                ):('')}
                            </>
                        )}
                        <div className="blockquote-footer" style={{verticalAlign:'bottom !important', color:'black'}} >
                        {this.props.autor}
                        </div>
                    </div>
                </div>
            </>
        );
    }
 }

 const MapStateToProps = state =>{
    return {
        user: state.user.nome,
        error: state.user.error,
        atualizaLista: state.user.atualizaLista
    };
  };
  const MapDispatchToProps = dispatch => {
    return{
        EnviouPost: () => dispatch({type:TiposAcoes.ENVIOU_POST}),
        AtualizouPost:() => dispatch({type:TiposAcoes.ATUALIZOU_POST}),
        ErroMensagem: (e) => dispatch({type:TiposAcoes.ERRO, erro: e})  
    };
  };
  
  export default connect(MapStateToProps,MapDispatchToProps)(LittleCard);