import React,{Component} from 'react';
import {Alert, Button} from 'react-bootstrap';

export class AlertaGenerico extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = { mostrar: this.props.mostrar };
    }
  
    render() {
      const handleHide = () => this.setState({ mostrar: false });
      const handlemostrar = () => this.setState({ mostrar: true });
      return (
        <>
          <Alert show={this.state.mostrar}  variant="success">
            <Alert.Heading>How's it going?!</Alert.Heading>
            <p>
              Duis mollis, est non commodo luctus, nisi erat porttitor ligula,
              eget lacinia odio sem nec elit. Cras mattis consectetur purus sit
              amet fermentum.
            </p>
            <hr />
            <div className="d-flex justify-content-end">
              <Button onClick={handleHide} variant="outline-success">
                Close me ya'll!
              </Button>
            </div>
          </Alert>
  
         
        </>
      );
    }
  }
  // {!this.state.mostrar && <Button onClick={handlemostrar}>mostrar Alert</Button>}