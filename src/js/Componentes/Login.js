import React from "react";
import {Form, Button, Alert, Dropdown, Spinner} from 'react-bootstrap' ;
import { connect } from 'react-redux';
import * as TiposAcoes from '../store/actions';
const REACT_APP_API_UXP = process.env.REACT_APP_API_UXP;

class Login extends React.Component{

  constructor(props)
  {
    super(props);
    this.state = { 
      login:'',
      senha:'', 
      message:'',
      load: false
    };
    
    this.LidandoMudancaTextoLogin = this.LidandoMudancaTextoLogin.bind(this);
    this.LidandoMudancaTextoSenha = this.LidandoMudancaTextoSenha.bind(this);
    this.IdentificandoEnter = this.IdentificandoEnter.bind(this);
  }
  IdentificandoEnter(e)
  {
    if(e.key === 'Enter')
    {
      this.Logando();
    }
  }
  signIn = () => {
    
    const data = { usuario: this.state.login, senha:this.state.senha };
    const requestInfo = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: new Headers({
            'Content-Type': 'application/json'
        }),
    };

    fetch(REACT_APP_API_UXP + '/login', requestInfo)
    .then(response => response.json())
    .then(resultado => {
      this.setState({load:false});
      if(resultado.autenticacao === true) {
        this.props.SingIn(this.state.login)
        this.setState({...this.state, login:'', senha:''})
        return true;
      }
      throw new Error("Login ou Senha inválidos...");
    })
    .catch(e => {
      this.setState({...this.state, message: e.message});
    }); 
}
  Logando = () => 
  {
    this.setState({load:true});
    if(this.state.login !== '' && this.state.senha !== '')
    {
      this.signIn();
      this.props.ErroMensagem(this.state.message);
    }else
    {
      setTimeout(()=>{
        this.props.ErroMensagem("Campo em Branco...");
        this.setState({load:false});
      }, 1000);      
    }
  }
  LidandoMudancaTextoLogin(e)
  {
    this.setState({login:e.target.value});
  }
  LidandoMudancaTextoSenha(e)
  {
    this.setState({senha: e.target.value});
  }
  render()
  {
    return(
      <>
        <div style={{float:'right'}}>
            {(this.props.user !== '')?(//Enquanto logado
            <div>
                <Button variant="outline-success">{this.props.user}</Button>
                <Button variant="success" onClick={this.props.Logout}>{'Logout'}</Button>
            </div>
            ):(//Enquanto deslogado
                <Dropdown drop={'left'}>
                    <Dropdown.Toggle variant="success">Logar</Dropdown.Toggle>
                
                    <Dropdown.Menu>
                    <div className={'container'} style={{minWidth:'20rem'}}>
                        <hr  className="my-3"/>
                                {
                                    this.props.error !== ''? (
                                        <Alert variant="danger" className="text-center"> {this.props.error} </Alert>
                                    ) : ''
                                }
                        <Form>
                            <Form.Group controlId="formBasicoLogin">
                            <Form.Label>Login</Form.Label>
                            <Form.Control placeholder="Entre com o Login" value={this.state.login} onChange={this.LidandoMudancaTextoLogin} onKeyDown={this.IdentificandoEnter}/>
                            </Form.Group>

                            <Form.Group controlId="formBasicoSenha">
                            <Form.Label>Senha</Form.Label>
                            <Form.Control type="password" placeholder="Entre com a Senha" value={this.state.senha} onChange={this.LidandoMudancaTextoSenha} onKeyDown={this.IdentificandoEnter}/>
                            </Form.Group>
                            <Button variant="primary" block type='button' onClick={this.Logando}>
                                    {(this.state.load)?(
                                      <Spinner as="span" animation="border" size="sm" role="status" aria-hidden="true" />
                                        ):('Entrar')}
                            </Button>
                        </Form>
                    </div>
                    </Dropdown.Menu>
                </Dropdown>
              )}
            
        </div>
      </>
    );
  }
}

const MapStateToProps = state =>{
  return {
    user: state.user.nome,
    error: state.user.error,
    listaAtualizada: state.user.listaAtualizada
  };
};
const MapDispatchToProps = dispatch => {
  return{
      SingIn: (login) => dispatch({type:TiposAcoes.LOGIN,login: login}),
      Logout: () => dispatch({type:TiposAcoes.LOGOUT}),
      ErroMensagem: (e) => dispatch({type:TiposAcoes.ERRO, erro: e})
  };
};

export default connect(MapStateToProps, MapDispatchToProps)(Login);