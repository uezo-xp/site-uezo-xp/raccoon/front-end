import React from 'react';
import {Provider} from 'react-redux';
import Routes from './js/routes/';
import Store from './js/store/'
import './css/App.css';


const App = () => (
    <Provider store={Store}>
        <Routes/>
    </Provider>
);

export default App;
